#!/bin/sh
if ( zenity --question --title="Network type" --text="Does your wifi network have a WPA password?" --ok-label="Yes" --cancel-label="No" ); then
    qrencode -l H -s 13 -o qr-wifi.png "WIFI:S:$(zenity --entry --text="Network name (SSID)" --title="Create WPA WiFi QR");T:WPA;P:$(zenity --password --title="WPA Password");;"
else
    qrencode -l H -s 13 -o qr-wifi.png "WIFI:S:$(zenity --entry --text="Network name (SSID)" --title="Create nopass WiFi QR");T:nopass;;"
fi
